'use strict';

angular.module('gallery', ['ngRoute', 'ngFileUpload', 'gallery.home', 'gallery.add'])
    .config(function($routeProvider) {
        $routeProvider.otherwise({redirectTo: '/home'});
    });
