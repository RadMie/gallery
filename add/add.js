angular.module('gallery.add', [])

    .config(function($routeProvider){

        $routeProvider.when('/add', {

            templateUrl: 'add/add.html',
            controller: 'AddController',
            controllerAs: 'addCtrl'

        })
    });