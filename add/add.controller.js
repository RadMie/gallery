angular.module('gallery.add')

    .controller('AddController', function (galleryService) {

        var vm = this;
        
        vm.notification = false;

        vm.sendImage = function(file) {
            vm.notification = false;
            galleryService.uploadImage(file)
                .then(function(res) {
                    if(res.data.error_code == 0) {
                        vm.notification = true;
                    }
                });
        }

    });


