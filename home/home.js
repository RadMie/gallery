angular.module('gallery.home', [])

    .config(function($routeProvider){

        $routeProvider.when('/home', {

            templateUrl: 'home/home.html',
            controller: 'HomeController',
            controllerAs: 'homeCtrl'

        })
    });