angular.module('gallery.home')

    .controller('HomeController', function(galleryService) {

        var vm = this;

        galleryService.getGallery()
            .then(function(res) {
                vm.images = res.data;
            });

    });