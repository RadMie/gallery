angular.module('gallery')

    .factory('galleryService', function($http, Upload) {

        var url = 'https://impaq.herokuapp.com/upload';

        function getGallery() {
            return $http.get(url);
        }

        function uploadImage(file) {
            return Upload.upload({
                        url: url,
                        data:{ file: file }
                    });
        }

        return {
            getGallery: getGallery,
            uploadImage: uploadImage
        }
    });